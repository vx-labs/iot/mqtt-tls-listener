package main

import (
	"context"
	"log"
	"net/http"
	_ "net/http/pprof"
	"os"
	"os/signal"
	"syscall"

	"github.com/sirupsen/logrus"
	"github.com/vx-labs/iot-mqtt-tls-listener/listener"
	"github.com/vx-labs/iot-mqtt-tls-listener/metrics"
)

func waitSignal() {
	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)
	<-sigc
}

func main() {
	logger := logrus.New()
	if os.Getenv("API_ENABLE_PROFILING") == "true" {
		go func() {
			logger.Println(http.ListenAndServe(":8080", nil))
		}()
	}
	ctx := context.Background()
	m := metrics.NewMetricHandler()
	mqtt := listener.NewListener(ctx)

	go serveHTTPHealth()
	waitSignal()
	logger.Infof("received interruption: closing broker")
	mqtt.Stop()
	m.Close()
}
func serveHTTPHealth() {
	mux := http.NewServeMux()
	mux.HandleFunc("/health", func(w http.ResponseWriter, _ *http.Request) {
		w.WriteHeader(http.StatusOK)
	})
	log.Println(http.ListenAndServe("[::]:9000", mux))
}
