package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"io"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"net/http"
	"github.com/sirupsen/logrus"
	"net"
)

var (
	ConnectedSessions = prometheus.NewGauge(prometheus.GaugeOpts{
		Subsystem: "listener",
		Namespace: "mqtt",
		Help:      "number of connected sessions",
		Name:      "connected_sessions",
	})
	PacketReceived = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name:      "packet_received",
		Help:      "number of mqtt packet received",
		Namespace: "mqtt",
		Subsystem: "listener",
	}, []string{"type"})
	PacketSent = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name:      "packet_sent",
		Help:      "number of mqtt packet sent",
		Namespace: "mqtt",
		Subsystem: "listener",
	}, []string{"type"})
	PacketDroppedReceived = prometheus.NewCounter(prometheus.CounterOpts{
		Name:      "dropped_packet",
		Help:      "number of mqtt packet dropped",
		Namespace: "mqtt",
		Subsystem: "listener",
	})
	ConnectionDropped = prometheus.NewCounterVec(prometheus.CounterOpts{
		Name:      "connection_dropped",
		Help:      "number of denied mqtt connection",
		Namespace: "mqtt",
		Subsystem: "listener",
	}, []string{"reason"})
)

func init() {
	prometheus.MustRegister(PacketReceived)
	prometheus.MustRegister(PacketSent)
	prometheus.MustRegister(PacketDroppedReceived)
	prometheus.MustRegister(ConnectedSessions)
	prometheus.MustRegister(ConnectionDropped)
}

func NewMetricHandler() io.Closer {
	http.Handle("/metrics", promhttp.Handler())
	listener, err := net.ListenTCP("tcp", &net.TCPAddr{
		Port: 8080,
		IP:   net.IPv6zero,
	})
	if err != nil {
		panic(err)
	}
	go func() {
		logrus.Error(http.Serve(listener, nil))
	}()
	return listener
}
