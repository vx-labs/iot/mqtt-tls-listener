job "tls-listener" {
  datacenters = ["dc1"]
  type = "service"
  constraint {
    operator  = "distinct_hosts"
    value     = "true"
  }
  update {
    max_parallel = 1
    min_healthy_time = "10s"
    healthy_deadline = "3m"
    health_check = "checks"
    auto_revert = true
    canary = 0
  }
  group "app" {
   vault {
      policies = ["nomad-tls-storer"]
      change_mode   = "signal"
      change_signal = "SIGUSR1"
      env = false
    }
    count = 3
    restart {
      attempts = 10
      interval = "5m"
      delay = "15s"
      mode = "delay"
    }
    task "tls-listener" {
      driver = "docker"
      env {
        CONSUL_HTTP_ADDR="172.17.0.1:8500"
        BROKER_HOST="172.17.0.1:4141"
        LE_EMAIL="julien@bonachera.fr"
      }
      config {
        image = "quay.io/vxlabs/iot-mqtt-tls-listener:v1.0.15"
        port_map {
          mqtts = 8883
          health = 9000
        }
      }
      resources {
        cpu    = 20
        memory = 64
        network {
          mbits = 10
          port "mqtts" {}
          port "health" {}
        }
      }
      service {
        name = "tls-listener"
        port = "mqtts"
        tags = ["urlprefix-:8883 proto=tcp"]
        check {
          type     = "http"
          path     = "/health"
          port     = "health"
          interval = "5s"
          timeout  = "2s"
        }
      }
    }
  }
}

