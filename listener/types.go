package listener

import (
	"context"
	"crypto/tls"
)

type TlsProvider interface {
	GetCertificate(ctx context.Context, cn string) ([]tls.Certificate, error)
}
