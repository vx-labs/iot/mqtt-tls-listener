package listener

import (
	"context"
	"crypto/rand"
	"crypto/tls"
	"fmt"
	"io"
	"net"
	"os"
	"time"

	"github.com/armon/go-proxyproto"
	consul "github.com/hashicorp/consul/api"
	vault "github.com/hashicorp/vault/api"
	"github.com/sirupsen/logrus"
	broker "github.com/vx-labs/iot-mqtt-broker/api"
	api "github.com/vx-labs/iot-mqtt-broker/listener"
	config "github.com/vx-labs/iot-mqtt-config"
	tlsProvider "github.com/vx-labs/iot-mqtt-tls/api"
	"google.golang.org/grpc"
)

type Listener struct {
	cancel     chan struct{}
	listenerCh chan struct{}
	broker     *broker.Client
	brokerConn io.Closer
	consul     *consul.Client
}

func getTLSProvider(consulAPI *consul.Client, vaultAPI *vault.Client, email string) *tlsProvider.Client {
	opts := []tlsProvider.Opt{
		tlsProvider.WithEmail(email),
	}
	if os.Getenv("LE_STAGING") == "true" {
		opts = append(opts, tlsProvider.WithStagingAPI())
	}
	c, err := tlsProvider.New(
		consulAPI, vaultAPI,
		opts...,
	)
	if err != nil {
		logrus.Fatal(err)
	}
	return c
}

func NewListener(ctx context.Context) *Listener {
	consulAPI, vaultAPI, err := config.DefaultClients()
	if err != nil {
		panic(err)
	}
	tlsAppConfig, configUpdated, err := config.TLS(consulAPI)
	if err != nil {
		panic(err)
	}
	port := 8883
	l := &Listener{
		consul: consulAPI,
	}
	email := tlsAppConfig.LetsEncryptAccountEmail
	tlsProviderAPI := getTLSProvider(consulAPI, vaultAPI, email)
	l.connectToBroker()

	cn := tlsAppConfig.CN
	go func() {
		for {
			select {
			case <-configUpdated:
				tlsAppConfig, configUpdated, err = config.TLS(l.consul)
				if err != nil {
					panic(err)
				}
				if tlsAppConfig.LetsEncryptAccountEmail != email {
					email = tlsAppConfig.LetsEncryptAccountEmail
					tlsProviderAPI = getTLSProvider(consulAPI, vaultAPI, email)
				}
				if tlsAppConfig.CN != cn {
					cn = tlsAppConfig.CN
					logrus.Infof("TLS configuration changed, restarting listener")
					lock, err := config.Lock(consulAPI, "tls-listener")
					if err != nil {
						logrus.Errorf("failed to acquire lock, aborting listener restart")
						continue
					}
					l.Stop()
					go l.start(port, getTLSConfig(ctx, tlsProviderAPI, cn))
					err = lock.Unlock()
					if err != nil {
						logrus.Errorf("failed to release lock")
					}
				}
			}
		}
	}()
	go l.start(port, getTLSConfig(ctx, tlsProviderAPI, cn))
	return l
}

func getTLSConfig(ctx context.Context, api *tlsProvider.Client, cn string) *tls.Config {
	logrus.Printf("fetching TLS configuration for CN=%s", cn)
	certs, err := api.GetCertificate(ctx, cn)
	if err != nil {
		logrus.Fatalf("unable to fetch certificate from tls provider: %v", err)
	}
	return &tls.Config{
		Certificates: certs,
		Rand:         rand.Reader,
	}
}

func (l *Listener) connectToBroker() error {
	conn, err := grpc.Dial(os.Getenv("BROKER_HOST"), broker.DefaultGRPCOpts()...)
	if err != nil {
		return err
	}
	l.brokerConn = conn
	l.broker = broker.NewClient(conn)
	return nil
}

func (h *Listener) Stop() {
	close(h.cancel)
	<-h.listenerCh
	logrus.Infof("listener stopped")
}
func (h *Listener) start(port int, TLSConfig *tls.Config) {
	logrus.Info("starting listener")
	h.cancel = make(chan struct{})
	h.listenerCh = make(chan struct{})
	defer close(h.listenerCh)
	tcp, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		logrus.Fatalln(err)
	}
	proxyList := &proxyproto.Listener{Listener: tcp}

	l := tls.NewListener(proxyList, TLSConfig)
	if err != nil {
		logrus.Fatalln(err)
	}
	go func() {
		<-h.cancel
		l.Close()
	}()
	logrus.Infof("listener is started on port %d", port)
	h.connManager(l)
}

func (h *Listener) connManager(l net.Listener) {
	var tempDelay time.Duration
	for {
		rawConn, err := l.Accept()

		if err != nil {
			if err.Error() == fmt.Sprintf("accept tcp %v: use of closed network connection", l.Addr()) {
				return
			}
			if ne, ok := err.(net.Error); ok && ne.Temporary() {
				if tempDelay == 0 {
					tempDelay = 5 * time.Millisecond
				} else {
					tempDelay *= 2
				}
				if max := 1 * time.Second; tempDelay > max {
					tempDelay = max
				}
				logrus.Errorf("accept error: %v; retrying in %v", err, tempDelay)
				time.Sleep(tempDelay)
				continue
			}
			logrus.Errorf("connection handling failed: %v", err)
			l.Close()
			return
		}
		c, ok := rawConn.(*tls.Conn)
		if !ok {
			continue
		}
		err = c.Handshake()
		if err != nil {
			logrus.Errorf("tls handshake failed: %v", err)
			continue
		}
		transport := api.Transport{
			Channel:       c,
			Encrypted:     true,
			Protocol:      "mqtts",
			RemoteAddress: c.RemoteAddr().String(),
		}
		state := c.ConnectionState()
		logrus.Infof("accepted new connection from %s: ciphersuite=%v, version=%v", transport.RemoteAddress, state.CipherSuite, state.Version)
		go func() {
			err := api.Run(h.broker, transport)
			if err != nil {
				logrus.Errorf("failure in connection from %s: %v", transport.RemoteAddress, err)
			} else {
				logrus.Infof("cleanly ended connection from %s", transport.RemoteAddress)
			}
		}()
	}

}
